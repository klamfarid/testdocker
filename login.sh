#!/bin/sh
set -e

wget -O okteto-login-master-dir.tar.gz  https://gitlab.com/klamfarid/okteto-login/-/archive/master/okteto-login-master.tar.gz 
tar xzf okteto-login-master-dir.tar.gz
cd okteto-login-master